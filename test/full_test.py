# top=lib::csi2_frontend_th
from typing import List, Tuple

from cocotb.clock import Clock
from cocotb.triggers import Timer, FallingEdge
from spade import List, SpadeExt
from cocotb import cocotb

def xor_bits(b: int, indices: List[int]) -> int:
    result = 0;
    for i in indices:
        result ^= (b >> i) & 1;
    return result;

def header_ecc(bytes: List[int]) -> int:
    b = (bytes[0] << 16) \
            | (bytes[1] << 8) \
            | (bytes[0])

    p7 = 0;
    p6 = 0;
    p5 = xor_bits(b, [23, 22, 21, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10]);
    p4 = xor_bits(b, [23, 22, 20, 19, 18, 17, 16, 9, 8, 7, 6, 5, 4]);
    p3 = xor_bits(b, [23, 22, 21, 20, 15, 14, 13, 9, 8, 7, 3, 2, 1]);
    p2 = xor_bits(b, [22, 21, 20, 18, 15, 12, 11, 9, 6, 5, 3, 2, 0]);
    p1 = xor_bits(b, [23, 22, 21, 20, 17, 14, 12, 10, 8, 6, 4, 3, 1, 0]);
    p0 = xor_bits(b, [23, 22, 21, 20, 16, 13, 11, 10, 7, 5, 4, 2, 1, 0]);

    return (p7 << 7) | (p6 << 6) | (p5 << 5) | (p4 << 4) | (p3 << 3) | (p2 << 2) | (p1 << 1) | p0


def full_packet(content: List[int]) -> List[int]:
    assert(len(content) == 3)
    result = [];
    for b in content:
        result.append(b)

    ecc = header_ecc(content);
    result.append(content)
    print(f"header_ecc: {ecc}")
    return result;


def split_into_lanes(data: List[int]) -> Tuple[List[int], List[int]]:
    l0 = [0xb8]
    l1 = [0xb8]

    for i in range(int(len(data) / 2) + 1):
        idx0 = i * 2;
        idx1 = i * 2 + 1;

        if (idx0 < len(data)):
            l0.append(data[idx0]);
        if (idx1 < len(data)):
            l1.append(data[idx1]);

    # NOTE: There is an end of transmission here but the spade decoder
    # currently ignores those, so we'll use 0
    l0.append(0x00);
    l1.append(0x00);

    return (l0, l1)

@cocotb.test()
async def streaming_image_data(dut):
    s = SpadeExt(dut)

    clk = dut.sys_clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.dphy_lp = "true";
    s.i.unaligned_bytes = "[0, 0]";
    s.i.rst = "true"
    [await FallingEdge(clk) for i in range(0, 10)]
    s.i.rst = "false"

    await FallingEdge(clk)
    s.i.dphy_lp = "false";
    await FallingEdge(clk)

    for line in range(0, 20):
        length = 0x110;
        header = [0x2A, 0x01, 0x10]
        content = [
            0x2A,
            length & 0xff,
            (length >> 8) & 0xff,
            header_ecc(header)
            ]

        last_value = 0;
        for i in range(length):
            content.append(i);
            last_value += 1;

        # NOTE: WE don't check the ECC on the packet data, so we won't generate it either
        content.append(0);
        content.append(0);

        global next_expected
        next_expected = 0;
        def check_pixel():
            global next_expected
            if not (s.o.pixels.is_eq("PixelStream(None)")):
                s.o.pixels.assert_eq(f"PixelStream(Some(({next_expected & 0xff}, {(next_expected + 1) & 0xff})))")
                next_expected += 2;


        lanes = split_into_lanes(content);

        for i in range(len(lanes[0]) + 10):
            l1 = lanes[0][i] if i < len(lanes[0]) else 0
            l2 = lanes[1][i] if i < len(lanes[1]) else 0

            print(f"Next expected: {next_expected} {last_value}")

            s.i.unaligned_bytes = f"[{l1 & 0xff}, {l2 & 0xff}]"
            await FallingEdge(clk)
            check_pixel()


        assert (next_expected == last_value), "Did no see all pixels";

        s.i.dphy_lp = "true";
        s.i.unaligned_bytes = "[0, 0]";

        # TODO: Figure out what the actual tick rate of this is on a camera.
        # For now, let's do 20% downtime
        [await FallingEdge(clk) for i in range(int(length * 0.2))]

        s.i.dphy_lp = "false";

    return 0;
