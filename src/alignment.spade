struct AlignDetectOut {
    alignment: Option<uint<4>>,
    unaligned: uint<16>
}

// Returns a non-sticky alignment, i.e. it is not kept after alignment is detected
entity align_detector(
    sys_clk: clock,
    rst: bool,
    unaligned_byte: uint<8>,
) -> AlignDetectOut {
    // reg(sys_clk) prev_byte = if !dphy_lp {unaligned_byte} else {0};
    reg(sys_clk) prev_byte = unaligned_byte;

    let unaligned = unaligned_byte `concat` prev_byte;

    // let sync_word: uint<5> = 0xb8 >> 3;
    let sync_word: uint<8> = 0b10111000;
    let alignment_now = match (
        trunc(unaligned >> 0) == sync_word,
        trunc(unaligned >> 1) == sync_word,
        trunc(unaligned >> 2) == sync_word,
        trunc(unaligned >> 3) == sync_word,
        trunc(unaligned >> 4) == sync_word,
        trunc(unaligned >> 5) == sync_word,
        trunc(unaligned >> 6) == sync_word,
        trunc(unaligned >> 7) == sync_word,
    ) {
        (true, false, false, false, false, false, false, false) => Some(0),
        (false, true, false, false, false, false, false, false) => Some(1),
        (false, false, true, false, false, false, false, false) => Some(2),
        (false, false, false, true, false, false, false, false) => Some(3),
        (false, false, false, false, true, false, false, false) => Some(4),
        (false, false, false, false, false, true, false, false) => Some(5),
        (false, false, false, false, false, false, true, false) => Some(6),
        (false, false, false, false, false, false, false, true) => Some(7),
        _ => None(),
    };

    AlignDetectOut $(
        alignment: alignment_now,
        unaligned
    )
}


entity byte_aligner(
    sys_clk: clock,
    dphy_lp: bool,
    primary: AlignDetectOut,
    secondary: Option<uint<4>>
) -> Option<uint<8>> {
    let alignment_now = match (primary.alignment, secondary) {
        (Some(prim), Some(_)) => Some(prim),
        (_, _) => None()
    };

    reg(sys_clk) sticky = if dphy_lp {
        None()
    }
    else {
        match (sticky, alignment_now) {
            (Some(sticky), _) => Some(sticky),
            (_, Some(new)) => Some(new),
            (None, None) => None(),
        }
    };

    match sticky {
        Some(a) => Some(trunc(primary.unaligned >> zext(a))),
        None => None()
    }
}



// Looks for alignment of both lanes, using the alignment of each unless
// one is unavailable in which case the other lane's alignment is used
entity lane_aligner(
    sys_clk: clock,
    rst: bool,
    dphy_lp: bool,
    lanes: [uint<8>; 2]
) -> (Option<uint<8>>, Option<uint<8>>) {
    let alignments = [
        inst align_detector$(sys_clk, rst, unaligned_byte: lanes[0]),
        inst align_detector$(sys_clk, rst, unaligned_byte: lanes[1]),
    ];

    (
        inst byte_aligner$(sys_clk, dphy_lp, primary: alignments[0], secondary: alignments[1].alignment),
        inst byte_aligner$(sys_clk, dphy_lp, primary: alignments[1], secondary: alignments[0].alignment),
    )
}
