/**
 * The MIT License
 * Copyright (c) 2016-2018 David Shah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*
 * Example CSI-2 receiver for iCE40
*/

`default_nettype none

module verilog_top #(
	parameter LANES = 2, // lane count
	parameter PAIRSWAP = 2'b10, // lane pair swap (inverts data for given  lane)

	parameter [1:0] VC = 2'b00, // MIPI CSI-2 "virtual channel"
	parameter [5:0] FS_DT = 6'h00, // Frame start data type
	parameter [5:0] FE_DT = 6'h01, // Frame end data type
	parameter [5:0] VIDEO_DT = 6'h2A, // Video payload data type (6'h2A = 8-bit raw, 6'h2B = 10-bit raw, 6'h2C = 12-bit raw)
	parameter [15:0] MAX_LEN = 8192 // Max expected packet len, used as timeout
)(
	input dphy_clk,
	// input [LANES-1:0] dphy_data_lane,
	input[LANES-1:0] din_raw0,
	input[LANES-1:0] din_raw1,
	input dphy_lp,

	input areset,

	output word_clk,
	output [31:0] payload_data,
	output payload_enable,
	output payload_frame,
);

	reg [1:0] div;
	always @(posedge dphy_clk or posedge areset)
		if (areset)
			div <= 0;
		else
			div <= div + 1'b1;
	assign word_clk = div[1];
	
	wire sreset;
	reg [7:0] sreset_ctr;
	always @(posedge word_clk or posedge areset)
		if (areset)
			sreset_ctr <= 0;
		else if (!(&sreset_ctr))
			sreset_ctr <= sreset_ctr + 1'b1;
			
	assign sreset = !(&sreset_ctr);
	
	wire byte_packet_done, wait_for_sync;
	wire [LANES*8-1:0] aligned_bytes;
	wire [LANES-1:0] aligned_bytes_valid;


	generate
	genvar ii;
	for (ii = 0; ii < LANES; ii++) begin
		wire [1:0] din_raw = {din_raw1[ii], din_raw0[ii]};

		wire [7:0] din_deser;
		dphy_iserdes #(
			.REG_INPUT(1'b1)
		) iserdes_i (
		   .dphy_clk(dphy_clk),
		   .din(din_raw),
		   .sys_clk(word_clk),
		   .areset(areset),
		   .dout(din_deser)
	    );

    	wire [7:0] din_deser_swap = PAIRSWAP[ii] ? ~din_deser : din_deser;

		dphy_rx_byte_align baligner_i (
			.clock(word_clk),
			.reset(sreset),
			.enable(1'b1),
			.deser_byte(din_deser_swap),
			.wait_for_sync(wait_for_sync),
			.packet_done(byte_packet_done),
			.valid_data(aligned_bytes_valid[ii]),
			.data_out(aligned_bytes[8*ii+7:8*ii])
		);

	end
	endgenerate

	wire [31:0] comb_word;
	wire comb_word_en, comb_word_frame;
	wire word_packet_done;

	dphy_rx_word_combiner #(
		.LANES(LANES)
	) combiner_i (
		.clock(word_clk),
		.reset(sreset),
		.enable(1'b1),
		.bytes_in(aligned_bytes),
		.bytes_valid(aligned_bytes_valid),
		.wait_for_sync(wait_for_sync),
		.packet_done(word_packet_done),
		.byte_packet_done(byte_packet_done),

		.word_out(comb_word),
		.word_enable(comb_word_en),
		.word_frame(comb_word_frame)
	);



	// For a fair comparison, don't connect these and let the synthesis tool
	// optimize away their generation, since the Spade code also does not
	// directly generate them
	wire vsync;
	wire in_line;
	wire in_frame;

	csi_rx_packet_handler #(
		.VC(VC),
		.FS_DT(FS_DT),
		.FE_DT(FE_DT),
		.VIDEO_DT(VIDEO_DT),
		.MAX_LEN(MAX_LEN)
	) handler_i (
		.clock(word_clk),
		.reset(sreset),
		.enable(1'b1),

		.data(comb_word),
		.data_enable(comb_word_en),
		.data_frame(comb_word_frame),

		.lp_detect(!dphy_lp),

		.sync_wait(wait_for_sync),
		.packet_done(word_packet_done),

		.payload(payload_data),
	 	.payload_enable(payload_enable),
		.payload_frame(payload_frame),

		.vsync(vsync),
		.in_frame(in_frame),
		.in_line(in_line)
	);
endmodule
